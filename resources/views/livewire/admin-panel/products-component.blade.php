<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="mb-0">
                    Data Produk
                    <a class="btn btn-success"
                    style="float:right;color:white" href="{{route('admin.products.create')}}">
                        Tambah Produk
                    </a>
                </h4>
                <br><br>
                <div class="table-responsive">
                    <input type="text" wire:model="search" class="form-control mb-2" placeholder="Cari produk..">
                    <table class="table table-borderless table-hover">
                        <thead class="bg-light">
                            <tr>
                                <th class="text-center"> # </th>
                                <th class="text-center">
                                    Gambar
                                </th>
                                <th class="text-center">
                                    Nama Produk
                                </th>
                                <th class="text-center">
                                    Harga
                                </th>
                                <th class="text-center">
                                    Deksripsi Produk
                                </th>
                                <th class="text-center">
                                    Kategori
                                </th>
                                <th class="text-center">
                                    Opsi
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr wire:loading>
                                <td colspan="7" class="text-center">
                                    Sedang mencari data produk..
                                </td>
                            </tr>
                            @php $no = 1 @endphp
                            @forelse($prdlist as $prd)
                            <tr wire:loading.remove>
                                <td class="text-center">
                                    {{ $no++ }}
                                </td>
                                <td class="text-center">
                                    <img src="{{ asset('uploads/products/'.$prd->id.'/'.$prd->image_name) }}"
                                    style="width:100px;height:100px;border-radius:10%;" alt="">
                                </td>
                                <td class="text-center">
                                    <h5>
                                        {{$prd->name}}
                                    </h5>
                                </td>
                                <td class="text-center">
                                    Rp {{ number_format($prd->discount,0,',','.') }}
                                    <del class="product-old-price">
                                        Rp {{ number_format($prd->price,0,',','.') }}
                                    </del>
                                </td>
                                <td class="text-center">
                                    {!! $prd->description !!}
                                </td>
                                <td class="text-center">
                                    <h6>
                                        {{$prd->category->name}}
                                    </h6>
                                </td>
                                <td class="text-center">
                                    <a href="{{route('admin.products.edit', ['id' => $prd->id])}}"
                                    class="btn btn-warning">
                                        Ubah
                                    </a>
                                    <a href="{{route('admin.products.delete', ['id' => $prd->id])}}"
                                    class="btn btn-danger">
                                        Hapus
                                    </a>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="7" class="text-center">
                                    Belum terdapat data produk
                                </td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            @if (count($prdlist) > 5)
                <div class="card-footer">
                    {{ $prdlist->links() }}
                </div>
            @endif
        </div>
    </div>
</div>
