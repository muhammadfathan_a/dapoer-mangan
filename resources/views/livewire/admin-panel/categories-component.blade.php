<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="row mx-auto mb-3 align-items-center" style="width: 100%;">
                    <div class="col-md-6 col-sm-12 px-0">
                        <h4 class="mb-0">
                            Data Kategori
                        </h4>
                    </div>
                    <div class="col-md-6 col-sm-12 px-0 text-right">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-success" data-toggle="modal"
                        data-target="#staticBackdrop">
                            Tambah Kategori
                        </button>
                        <!-- Modal -->
                        <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false"
                        tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content bg-white">
                                    <div class="modal-header">
                                        <h4 class="card-title mb-0">
                                            Tambah Kategori
                                        </h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form class="forms-sample" method="post" id="cat_form" enctype="multipart/form-data" >
                                    {{csrf_field()}}
                                        <div class="modal-body">
                                            <div class="form-group row">
                                                <label for="exampleInputEmail2" class="col-sm-3 col-form-label">
                                                    Nama
                                                </label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="Name" id="Name"
                                                    placeholder="Nama kategori produk">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="exampleInputPassword2" class="col-sm-3 col-form-label">
                                                    Keterangan
                                                </label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="Type" id="Type"
                                                    placeholder="Keterangan kategori produk">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">
                                                    Thumbnail
                                                </label>
                                                <div class="col-sm-9 text-left">
                                                    <img style="border-radius: 5px;" id="imageHolder" src="" class="my-2"
                                                    alt="upload gambar" height="300" width="300"/>
                                                    <br>
                                                    <input type="file" name="inp_files" id="inp_files" required>
                                                    <br>
                                                    <div id="empty_image"></div>
                                                    <input id="inp_img" name="img" type="hidden" value="">
                                                </div>
                                            </div>
                                            @if($errors->any())
                                            <ul>
                                                @foreach($errors->all() as $err)
                                                <tr>
                                                    <td>
                                                        <li>{{$err}}</li>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </ul>
                                            @endif
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success mr-2">
                                                Simpan
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <input type="text" wire:model="search" class="form-control mb-2" placeholder="Cari kategori..">
                    <table class="table table-hover table-borderless">
                        <thead class="bg-light">
                            <tr>
                                <th class="text-center">
                                    #
                                </th>
                                <th class="text-center">
                                    Thumbnail
                                </th>
                                <th>
                                    Nama Kategori
                                </th>
                                <th>
                                    Keterangan
                                </th>
                                <th class="text-center">
                                    Opsi
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr wire:loading>
                                <td colspan="5" class="text-center">
                                    Sedang mencari data kategori..
                                </td>
                            </tr>
                            @php $no = 1; @endphp
                            @forelse($catlist as $cat)
                            <tr wire:loading.remove>
                                <td class="text-center">
                                    {{ $no++ }}
                                </td>
                                <td class="text-center">
                                    <img src="{{ asset($cat->thumbnail) }}" style="zoom: 300%;" class="rounded">
                                </td>
                                <td>
                                    {{ $cat->name }}
                                </td>
                                <td>
                                    {{$cat->type}}
                                </td>
                                <td align="center">
                                    <a href="{{route('admin.categories.edit', ['id' => $cat->id])}}"
                                    class="btn btn-warning">Edit</a>
                                    <a href="{{route('admin.categories.delete', ['id' => $cat->id])}}"
                                    onclick="delete()" class="btn btn-danger">Delete</a>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="5" class="text-center">
                                    Belum terdapat data kategori
                                </td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            @if (count($catlist) > 5)
                <div class="card-footer">
                    {{ $catlist->links() }}
                </div>
            @endif
        </div>
    </div>
</div>
