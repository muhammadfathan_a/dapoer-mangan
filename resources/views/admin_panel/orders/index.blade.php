@extends('admin_panel.adminLayout') @section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4>
                        Manajemen Pesanan <small> Terbaru </small>
                    </h4>
                    <div class="table-responsive mt-5 mb-0">
                        @foreach($sale as $s)
                            @foreach($all as $c)
                                @if($c[0]==$s->id)
                                    @foreach($products as $p)
                                        @if($p)
                                            @if($c[1]==$p->id)
                                                @php
                                                    $index[] = [$s->id, $s->user_id];
                                                    // $unique = array_unique($index);
                                                    $number = count($index);
                                                @endphp
                                            @break
                                            @endif
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        @endforeach
                        <div style="
                            margin: auto;
                            display: flex;
                            flex-wrap: wrap;
                            justify-content: space-around;
                        ">
                            <div class="row mx-auto" style="width: 100%;">
                                @foreach($users as $u)
                                    @foreach ($sale as $s)
                                        @if ($u['id'] == $s->user_id)
                                            @php
                                                // $user[$u['id']] = $u['full_name'];
                                                // $userList = array_unique($user);
                                                $userId[$u['id']] = $u['id'];
                                                $userName[$u['id']] = $u['full_name'];
                                                $userIdList = array_unique($userId);
                                                $userNameList = array_unique($userName);
                                            @endphp
                                        @endif
                                    @endforeach
                                @endforeach
                                @php
                                    $realUser = array();
                                    foreach ($userIdList as $key => $idListVal)
                                    {
                                        $nameListVal = $userNameList[$key];
                                        $realUser[$key] = [$idListVal, $nameListVal];
                                    }
                                @endphp
                                @foreach ($realUser as $name)
                                    <div class="col-sm-12 col-md-12 mt-4 mb-3 px-0">
                                        <div class="card mx-auto" style="width: 97%;">
                                            <div class="card-header">
                                                <h6>
                                                    <b> {{ $name[1] }} </b>
                                                </h6>
                                            </div>
                                            <div class="card-body px-0">
                                                <div class="row mx-auto" style="width: 100%;">
                                                    @php
                                                        foreach ($index as $entry) {
                                                            // if an entry for this user id hasn't
                                                            // been created in the result, add this object
                                                            if (!isset($result[$entry[0]])) {
                                                                $result[$entry[0]] = $entry;

                                                            // otherwise, iterate this object and
                                                            // add the values of its keys to the existing entry
                                                            } else {
                                                                foreach ($entry as $key => $value) {
                                                                    $result[$entry[0]][$key] = $value;
                                                                }
                                                            }
                                                        }
                                                    @endphp
                                                    @foreach ($result as $id)
                                                        @if ($id[1] == $name[0])
                                                            <div class="col-sm-12 col-md-3 text-center">
                                                                <div class="row mx-auto" style="width: 100%;">
                                                                    @foreach ($sale as $jual)
                                                                        @if ($id[0] == $jual->id)
                                                                            <div class="col-sm-12 col-md-12 text-left mt-5 mb-3">
                                                                                {{-- No. Transaksi --}}
                                                                                <b>
                                                                                    INV.{{
                                                                                        $name[0].'/'.
                                                                                        Carbon\Carbon::parse($u['created_at'])->format('y').
                                                                                        '/'.sprintf("%04d", $id[0])
                                                                                    }}
                                                                                </b>
                                                                                <br>
                                                                                Status Pesanan:
                                                                                <b style="text-transform: capitalize;">
                                                                                    {{ $jual->order_status }}
                                                                                </b>
                                                                                <br>
                                                                                <small>
                                                                                    Tgl Pemesanan: {{ Carbon\Carbon::parse($jual->created_at) }}
                                                                                </small>
                                                                                @php
                                                                                    if ($jual->order_status == 'ditempatkan') { $color = 'btn-info'; }
                                                                                    else if($jual->order_status == 'diproses') { $color = 'btn-warning'; }
                                                                                    else if($jual->order_status == 'diterima') { $color = 'btn-success'; }
                                                                                    else if($jual->order_status == 'batal') { $color = 'btn-danger'; }
                                                                                @endphp
                                                                                {{-- button --}}
                                                                                <button type="button" class="btn btn-sm {{ $color }}" data-toggle="modal"
                                                                                data-target="#historyModal-{{ $id[0] }}" style="
                                                                                    margin: 10px 0px;
                                                                                    line-height: 25px;
                                                                                ">
                                                                                    Lihat/Ubah Status
                                                                                    {{-- <i class="fa fa-arrows-alt" style="margin: 0px 5px;"></i> --}}
                                                                                </button>
                                                                            </div>
                                                                        @endif
                                                                    @endforeach
                                                                </div>
                                                                <!-- Modal -->
                                                                <div class="modal fade" id="historyModal-{{ $id[0] }}" tabindex="-1"
                                                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                    <div class="modal-dialog modal-dialog-centered" style="
                                                                        max-width: none;
                                                                        width: max-content;
                                                                    ">
                                                                        <div class="modal-content bg-white">
                                                                            <div class="modal-header">
                                                                                <div style="display: flex; justify-content: space-between;">
                                                                                    <h5 class="modal-title" id="exampleModalLabel">
                                                                                        INV.{{
                                                                                            $name[0].'/'.
                                                                                            Carbon\Carbon::parse($u['created_at'])->format('y').
                                                                                            '/00'.sprintf("%04d", $id[0])
                                                                                        }}
                                                                                    </h5>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <table class="table table-striped table-border" style="margin-bottom: 0px;">
                                                                                    <thead class="bg-danger text-white">
                                                                                        <th class="text-center"> Gambar </th>
                                                                                        <th class="text-center"> Nama </th>
                                                                                        <th class="text-center"> Varian </th>
                                                                                        <th class="text-center"> Jumlah </th>
                                                                                        <th class="text-center"> Harga Satuan </th>
                                                                                        <th class="text-center"> Harga Total </th>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    @foreach($sale as $s)
                                                                                        @foreach($all as $c)
                                                                                            @if($c[0]==$s->id)
                                                                                                @foreach($products as $p)
                                                                                                    @if($p)
                                                                                                        @if($c[1]==$p->id)
                                                                                                            @if ($s->id == $id[0])
                                                                                                                @php
                                                                                                                    $getProductId[] = $s->id;
                                                                                                                    $orderStatus[$s->id] = $s->order_status;
                                                                                                                @endphp
                                                                                                                <tr>
                                                                                                                    <td class="text-center" align="center">
                                                                                                                        <img src="{{ asset('uploads/products/'.$p->id.'/'.$p->image_name)  }}"
                                                                                                                        height="120px" width="120px" style="zoom: 200%;">
                                                                                                                    </td>
                                                                                                                    <td class="text-center" style="vertical-align: middle;">
                                                                                                                        {{ $p->name }}
                                                                                                                    </td>
                                                                                                                    <td class="text-center" style="vertical-align: middle;">
                                                                                                                        <span class="badge badge-warning text-dark"
                                                                                                                        style="font-size: 11px; font-weight: 500;">
                                                                                                                            {{ $c[3] }}
                                                                                                                        </span>
                                                                                                                    </td>
                                                                                                                    <td class="text-center" style="vertical-align: middle;">
                                                                                                                        {{ $c[2] }}
                                                                                                                    </td>
                                                                                                                    <td class="text-center" style="vertical-align: middle;">
                                                                                                                        Rp {{ number_format($p->discount,0,',','.') }}
                                                                                                                    </td>
                                                                                                                    @php
                                                                                                                        $priceSum = $p->discount * $c[2];
                                                                                                                        $priceToPay[] = $priceSum;
                                                                                                                    @endphp
                                                                                                                    <td class="text-right" style="vertical-align: middle;">
                                                                                                                        Rp {{ number_format($priceSum,0,',','.') }}
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            @endif
                                                                                                        @break
                                                                                                        @endif
                                                                                                    @endif
                                                                                                @endforeach
                                                                                            @endif
                                                                                        @endforeach
                                                                                    @endforeach
                                                                                    @foreach (array_unique($getProductId) as $item)
                                                                                        @if ($id[0] == $item)
                                                                                            @foreach($sale as $s)
                                                                                                @if ($item == $s->id)
                                                                                                <tr>
                                                                                                    <th colspan="5" class="text-left" style="vertical-align: middle;">
                                                                                                        Ongkir
                                                                                                    </th>
                                                                                                    <td class="text-right" style="vertical-align: middle;">
                                                                                                        Rp 10.000
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th colspan="5" class="text-left" style="vertical-align: middle;">
                                                                                                        Harga Total Pembayaran
                                                                                                    </th>
                                                                                                    <th class="text-right" style="vertical-align: middle;">
                                                                                                        Rp {{ number_format($s->price + 10000,0,',','.') }}
                                                                                                    </th>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="text-right" colspan="10">
                                                                                                        <form method="post" style="
                                                                                                            display: inline-block;
                                                                                                            width: 100%;
                                                                                                            margin: auto;
                                                                                                        ">
                                                                                                            {{csrf_field()}}
                                                                                                            <div class="row" style="align-items: center; margin: auto;">
                                                                                                                <div class="col-sm-6 col-md-6 px-0">
                                                                                                                    <div class="form-group mb-0" style="float: left; display: flex;">
                                                                                                                        <h6 class="mb-0">
                                                                                                                            Ubah Status
                                                                                                                        </h6>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="col-sm-6 col-md-6 px-0">
                                                                                                                    <div class="form-group mb-0" style="float: right; display: flex;">
                                                                                                                        <input type="hidden" value="{{$s->id}}" name="orderId">
                                                                                                                        <select name="stat" class="form-control" style="
                                                                                                                            width: max-content !important;
                                                                                                                            margin: 0px 10px;
                                                                                                                        ">
                                                                                                                            <option disabled> Status Pesanan </option>
                                                                                                                            @foreach($status as $x)
                                                                                                                                @if($s->order_status != $x)
                                                                                                                                    <option value="{{$x}}">
                                                                                                                                        {{$x}}
                                                                                                                                    </option>
                                                                                                                                @endif
                                                                                                                            @endforeach
                                                                                                                        </select>
                                                                                                                        <input type="submit" class="btn btn-sm btn-dark" value="Perbarui">
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </form>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                @endif
                                                                                            @endforeach
                                                                                        @endif
                                                                                    @endforeach
                                                                                </tbody>
                                                                            </table>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <span style="
                                                                                    width: 100%;
                                                                                    text-align: left;
                                                                                    ">
                                                                                    Status Pemesanan
                                                                                    <b style="text-transform: capitalize;">
                                                                                        @foreach ($orderStatus as $key => $oS)
                                                                                            @if ($key == $id[0])
                                                                                            @endif
                                                                                        @endforeach
                                                                                        {{ $oS }}
                                                                                    </b>
                                                                                </span>
                                                                                <button class="btn btn-danger" data-dismiss="modal">
                                                                                    Tutup
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="mt-5">
                                {{ $users->appends($_GET)->links('pagination::bootstrap-4') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
