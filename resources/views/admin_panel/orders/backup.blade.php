<table class="table table-hover">
    <thead>
        <tr>
            <th class="text-center">
                No. Transaksi
            </th>
            <th class="text-center">
                Nama Lengkap
            </th>
            <th class="text-center">
                Alamat Lengkap
            </th>
            <th class="text-center">
                Produk
            </th>
            <th class="text-center">
                Jumlah
            </th>
            <th class="text-center">
                Varian
            </th>
            <th class="text-center">
                Pemesanan Tanggal
            </th>
            <th class="text-center">
                Status Pesanan
            </th>
            <th class="text-center">
                Opsi
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach($sale as $s)
            @foreach($all as $c)
                @if($c[0]==$s->id)
                    @foreach($products as $p)
                        @if($p)
                            @if($c[1]==$p->id)
                                <tr>
                                    <td class="text-center">{{$s->user_id.$s->id}}</td>
                                    @foreach($users as $u)
                                        @if($u->id == $s->user_id)
                                            <td class="text-center">{{$u->full_name}}</td>
                                            <td class="text-center">{{$u->area}}, {{$u->city}}, {{$u->zip}} ,Bangladesh</td>
                                        @break
                                        @endif
                                    @endforeach
                                    <td class="text-center">
                                        {{$p->name}}
                                    </td>
                                    <td class="text-center">
                                        {{$c[2]}}
                                    </td>
                                    <td class="text-center">
                                        <div style="height:25px;width:25px;margin:5px;
                                        display:inline-block;background-color: {{$c[3]}}">
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        {{$s->created_at}}
                                    </td>
                                    <td class="text-center">
                                    {{$s->order_status}}
                                    </td>
                                    <td class="text-center">
                                        <form method="post" style="display:inline-block">
                                            {{csrf_field()}}
                                            <input type="hidden" value="{{$s->id}}" name="orderId">
                                            <select name="stat">
                                                @foreach($status as $x)
                                                    @if($s->order_status!=$x)
                                                        <option value="{{$x}}">{{$x}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            <input type="submit" class="btn btn-sm btn-warning" value="Update">
                                        </form>
                                    </td>
                                    @break
                                    @endif
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                @endforeach
            </tr>
    </tbody>
</table>
