@extends('admin_panel.adminLayout') @section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12 d-flex align-items-stretch grid-margin">
            <div class="row flex-grow">
                <div class="col-12">
                    <div class="card" style="
                        border-radius: 10px;
                        box-shadow: 3px 3px 7px 3px #ccc;
                    ">
                        <div class="card-body">
                        <a href="{{route('admin.categories')}}"> < Kembali ke Kategori </a>
                        <br><br>
                        <h4 class="card-title"> Hapus Kategori </h4>
                        <br>
                        <form class="forms-sample" method="post">
                        {{csrf_field()}}
                            <div class="form-group">
                                <label for="exampleInputEmail1"> Nama </label>
                                <input type="text" class="form-control" id="exampleInputEmail1" name="Name" value="{{$category->name}}" disabled>
                            </div>
                            <div class="form-group">
                            <label for="exampleInputPassword1"> Keterangan </label>
                                <textarea type="textarea" class="form-control" name="Type" disabled>{{$category->type}}</textarea>
                            </div>
                            <div class="form-group">
                                <label class="exampleInputPassword1">
                                    Thumbnail
                                </label><br>
                                <img style="border-radius: 5px;" id="imageHolder" src="{{ asset($category->thumbnail) }}" class="my-2"
                                alt="upload gambar" height="300" width="300"/>
                                <input type="hidden" name="del_img" id="del_img" value="{{ $category->thumbnail }}" >
                            </div>
                            <input  type="submit" name="updateButton"  class="btn btn-danger mr-2" id="updateButton" value="DELETE" />
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
