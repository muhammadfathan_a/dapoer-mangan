@extends('admin_panel.adminLayout') @section('content')
<script src="{{asset('js/lib/jquery.js')}}"></script>
<script src="{{asset('js/dist/jquery.validate.js')}}"></script>
<style>label.error {
color: #a94442;
background-color: #f2dede;
border-color: #ebccd1;
padding:1px 20px 1px 20px;
}</style>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12 d-flex align-items-stretch grid-margin">
            <div class="row flex-grow">
                <div class="col-12">
                    <div class="card" style="
                        border-radius: 10px;
                        box-shadow: 3px 3px 7px 3px #ccc;
                    ">
                        <div class="card-body">
                            <a class="text-danger" href="{{route('admin.categories')}}"> < Kembali ke Kategori </a>
                            <br><br>
                            <form class="forms-sample" method="post" id="cat_form" enctype="multipart/form-data">
                            {{csrf_field()}}
                                <div class="form-group mb-5">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6 text-left">
                                            <h3 class="mb-0"> Ubah Kategori </h3>
                                        </div>
                                        <div class="col-sm-12 col-md-6 text-right">
                                            <input type="submit" name="updateButton"  class="btn btn-success mr-2"
                                            id="updateButton" value="Simpan Perubahan" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label for="exampleInputEmail1"> Nama </label>
                                    <input type="text" class="form-control" id="Name" name="Name" value="{{$category->name}}">
                                </div>
                                <div class="form-group mb-4">
                                    <label for="exampleInputPassword1"> Keterangan </label>
                                    <textarea type="textarea" class="form-control" id="Type" name="Type" rows="5">{{$category->type}}</textarea>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-12 col-form-label">
                                        Thumbnail
                                    </label>
                                    <div class="col-sm-12 text-left">
                                        @if ($category->thumbnail != NULL)
                                            <img style="border-radius: 5px;" id="imageHolder" src="{{ asset($category->thumbnail) }}" class="my-2"
                                            alt="upload gambar" height="300" width="300"/>
                                        @else
                                            <img style="border-radius: 5px;" id="imageHolder" src="" class="my-2"
                                            alt="upload gambar" height="300" width="300"/>
                                        @endif
                                        <br>
                                        <input type="file" name="inp_files" id="inp_files">
                                        <br>
                                        <div id="empty_image"></div>
                                        <input id="inp_img" name="img" type="hidden" value="">
                                        <input type="hidden" name="del_img" id="del_img" value="{{ $category->thumbnail }}" >
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function fileChange(e) {

        document.getElementById('inp_img').value = '';

        for (var i = 0; i < e.target.files.length; i++) {

            var file = e.target.files[i];

            if (file.type == "image/jpeg" || file.type == "image/png") {

                var reader = new FileReader();
                reader.onload = function(readerEvent) {

                    var image = new Image();
                    image.onload = function(imageEvent) {

                    var max_size = 600;
                    var w = image.width;
                    var h = image.height;

                    if (w > h) {  if (w > max_size) { h*=max_size/w; w=max_size; }
                    } else     {  if (h > max_size) { w*=max_size/h; h=max_size; } }

                    var canvas = document.createElement('canvas');
                    canvas.width = w;
                    canvas.height = h;
                    canvas.getContext('2d').drawImage(image, 0, 0, w, h);
                    if (file.type == "image/jpeg") {
                        var dataURL = canvas.toDataURL("image/jpeg", 1.0);
                    } else {
                        var dataURL = canvas.toDataURL("image/png");
                    }
                    document.getElementById('inp_img').value += dataURL + '|';
                    }
                    image.src = readerEvent.target.result;
                }
                reader.readAsDataURL(file);

                readURL(this);

            } else {
                document.getElementById('inp_files').value = '';
                alert('Please only select images in JPG or PNG format.');
                return false;
            }
        }
    }
    document.getElementById('inp_files').addEventListener('change', fileChange, false);
</script>
<script>
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imageHolder').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<!--JQUERY Validation-->
<script>
    $(document).ready(function() {
        // validate the comment form when it is submitted
        //$("#commentForm").validate();
        // validate signup form on keyup and submit
        $("#cat_form").validate({
            rules: {
                Name: "required",
                Type: "required",
            },
            messages: {
                Name: "Masukkan nama kategori",
                Type: "Masukkan keterangan kategori",

            }
        });
    });
    </script>
<!--/JQUERY Validation-->
@endsection
