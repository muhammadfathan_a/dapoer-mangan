@extends('store.storeLayout')
@section('content')
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row" style="
                background: #fff;
                border-radius: 20px;
                box-shadow: 0px 0px 15px -5px #333;
                padding: 20px 20px;
            ">

                <!-- section title -->
                <div class="col-md-12">
                    <div class="section-title">
                        <h2 class="title" style="text-transform: capitalize;"> Butuh Bantuan? </h2>
                    </div>
                </div>
                <!-- /section title -->

                <!-- Products tab & slick -->
                <div class="col-md-12" style="margin-top: 55px;">
                    <div class="card">
                        <div class="card-body" style="padding: 0px 25px;">
                            <div class="media">
                                <div class="media-body">
                                    <div class="card-text" style="display: flex;">
                                        <div>
                                            <i class="fa fa-star icon-md text-info d-flex align-self-start mr-3"
                                            style="margin: 0px 5px; color: gold;"></i>
                                        </div>
                                        <div>
                                            Customer Assistant kami akan selalu siap membantu Anda dengan sepenuh hati
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-body">
                                    <div class="card-text" style="display: flex;">
                                        <div>
                                            <i class="fa fa-star icon-md text-info d-flex align-self-start mr-3"
                                            style="margin: 0px 5px; color: gold;"></i>
                                        </div>
                                        <div>
                                            Menjawab pertanyaan Anda seputar template dan layanan kami
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-body">
                                    <div class="card-text" style="display: flex;">
                                        <div>
                                            <i class="fa fa-star icon-md text-info d-flex align-self-start mr-3"
                                            style="margin: 0px 5px; color: gold;"></i>
                                        </div>
                                        <div>
                                            Menentukan template atau layanan yang tepat untuk Anda
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-body">
                                    <div class="card-text" style="display: flex;">
                                        <div>
                                            <i class="fa fa-star icon-md text-info d-flex align-self-start mr-3"
                                            style="margin: 0px 5px; color: gold;"></i>
                                        </div>
                                        <div>
                                            Membantu proses pembelian/order template atau layanan kami
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-body">
                                    <div class="card-text" style="display: flex;">
                                        <div>
                                            <i class="fa fa-star icon-md text-info d-flex align-self-start mr-3"
                                            style="margin: 0px 5px; color: gold;"></i>
                                        </div>
                                        <div>
                                            Melayani dengan sabar, menyenangkan dan tanggung jawab
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer" style="margin: 45px 0px;">
                            <a href="mailto: muhammadfathan23@gmail.com" class="text-primary btn"
                            style="text-decoration: underline; font-weight: 600;">
                                Email
                            </a>
                            <a href="https://wa.me/+6289617341858?text=Permisi%20..%20Saya%20ingin%20memesan%20....%20"
                            target="_blank" class="btn text-success" style="text-decoration: underline; font-weight: 600;">
                                Chat via WhatsApp
                            </a>
                        </div>
                    </div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
    </div>
@endsection
