@extends('store.storeLayout')
@section('content')
<script src="{{asset('js/lib/jquery.js')}}"></script>
<script src="{{asset('js/dist/jquery.validate.js')}}"></script>
<script data-require="jquery@3.1.1" data-semver="3.1.1" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<link type="text/css" rel="stylesheet" href="{{asset('css/style_for_quantity.css')}}" />

<style>
label.error {
color: #a94442;
background-color: #f2dede;
border-color: #ebccd1;
padding:1px 20px 1px 20px;
}


</style>

<!-- SECTION -->
<div class="section">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- Product main img -->
            <div class="col-md-7">
                <div id="product-main-img">
                    <div class="product-preview">
                        <img src="{{ asset('uploads/products/'.$product->id.'/'.$product->image_name) }}" style="
                            border-radius: 10px;
                        ">
                    </div>
                </div>
            </div>
            <!-- /Product main img -->
            <!-- Product details -->
            <div class="col-md-5" style="
                background: #fff;
                box-shadow: 0px 0px 15px -5px #333;
                border-radius: 20px;
                padding: 10px 25px;
            ">
                <div class="product-details" style="margin: 15px 0px;">
                    <h2 class="product-name">{{$product->name}}</h2>
                    <div>
                        <div class="product-rating">
                            <i class="fa fa-star" style="color: gold;"></i>
                            <i class="fa fa-star" style="color: gold;"></i>
                            <i class="fa fa-star" style="color: gold;"></i>
                            <i class="fa fa-star" style="color: gold;"></i>
                            <i class="fa fa-star" style="color: gold;"></i>
                        </div>
                    </div>
                    <div>
                        <h3 class="product-price">Rp {{number_format($product->discount,0,',','.')}}
                            <del class="product-old-price">
                                Rp {{$product->price}}
                            </del>
                        </h3>
                        <span class="product-available" style="text-transform: capitalize;">
                            Ready
                            <small style="font-weight: 500;"> Sist/Gan! </small>
                        </span>
                    </div>
                    <div class="card" style="margin-top: 45px; min-height: 70px;">
                        <div class="card-header text-danger" style="font-weight: 700;">
                            Deskripsi
                        </div>
                        <div class="card-body">
                            <p>{!!$product->description!!}</p>
                        </div>
                    </div>
                    <form method="post" id="order_form">
                    {{csrf_field()}}
                    <div class="product-options">
                        <div class="form-group">
                            <input type="hidden" id="discount_price_holder" name="discount_price_holder" value={{number_format($product->discount,0,',','.')}}>
                            <label>
                                <div id="field1" style="line-height: 35px;">
                                    <span class="text-danger" style="font-weight: 700; text-transform: capitalize;">
                                        Jumlah
                                    </span><br>
                                    <button type="button" id="sub" class="sub" style="
                                        background: #ef273c;
                                        border: none !important;
                                        border-radius: 5px;
                                        color: #fff;
                                    ">
                                        -
                                    </button>
                                    <input class="text-center" type="number" id="quantity" name="quantity"
                                    value="1" min="1" max="100"  style="border-radius: 5px; border: 1px solid #000;"/>
                                    <button type="button" id="add" class="add" style="
                                        background: #ef273c;
                                        border: none !important;
                                        border-radius: 5px;
                                        color: #fff;
                                    ">
                                        +
                                    </button>
                                </div>
                            </label>
                        </div>

                        <div class="form-group" style="margin-top: 30px; line-height: 35px;">
                            <label for="choice" class="text-danger" style="font-weight: 700;">
                                Pilihan
                            </label><br>

                            @php $no = 1; @endphp
                            <div style="display: flex; flex-wrap: wrap;">
                                @foreach($choices as $c)
                                    <div>
                                        <input id={{ $no }} type="radio" name="color" value="{{$c}}">
                                        <label for="{{ $no }}">
                                            {{ $c }}
                                        </label>
                                    </div>
                                @php $no++ @endphp
                                @endforeach
                            </div>
                        </div>

                    </div>
                    <div id="for_error"></div>
                    <div class="add-to-cart">
                        <button type="submit" name="myButton" id="myButton" class="add-to-cart-btn">
                            <i class="fa fa-shopping-cart" style="left: 10px;"></i>
                            + Keranjang
                        </button>
                    </div>
                    </form>
                    <ul class="product-links" style="align-content: center;">
                        <li>Kategori</li>
                        <li>
                            <a class="badge badge-success" href="{{route('user.search')}}?c={{$product->category->id}}">
                                {{$product->category->name}}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /Product details -->

        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</div>
<div style="height:200px"></div>

<!--JQUERY Validation-->
<script>

    $(document).ready(function() {
        $("#order_form").validate({
            submitHandler: function (form) {
                if($('input[name=color]:checked').val() == undefined)
                {
                    document.getElementById("for_error").innerHTML =
                    "<label class='error' style=' '> Pilih jenis atau varian terlebih dahulu </label>";
                }
                else
                {
                    return true;
                }
            }
        });
    });

    $('.add').click(function () {
        $(this).prev().val(+$(this).prev().val() + 1);
    });

    $('.sub').click(function () {
        if ($(this).next().val() > 1) {
            $(this).next().val(+$(this).next().val() - 1);
        }
    });



    </script>
<!--/JQUERY Validation-->
<!-- /SECTION -->
@endsection
