<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>
        {{ ucwords(str_replace('-', ' ', Request::segment(1))) }} | Dapoer Mangan
    </title>

    <link rel="shortcut icon" href="{{ asset('img/dapoer-mangan/logo.png') }}" />

    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" />

    <!-- Slick -->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/slick.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('css/slick-theme.css') }}" />

    <!-- nouislider -->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/nouislider.min.css') }}" />

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/style2.css') }}" />

    <!-- JQuery and Validator Plugins -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    {{-- custom css --}}

    <style>
        @media only screen and (max-width: 767px){
            #head_links {
                visibility: hidden;
            }
            .custom_search_top {
                text-align:center;
            }

            .header-ctn {
                width: 100%;
            }
            .pull-left, .pull-right {
                text-align: center !important;
                margin: 10px auto;
            }
            .img-logo {
                margin: 25px 0px;
            }
            .nav .open>a, .nav .open>a:focus, .nav .open>a:hover {
                background-color: transparent !important;
                border-color: transparent !important;
            }
            .header-search form .input {
                width: calc(100% - 145px);
            }
            #user-account {
                display: contents !important;
            }
            #logout-btn {
                color: #000;
                font-weight: bold;
                border: none !important;
            }
            .container {
                padding-right: 25px !important;
                padding-left: 25px !important;
            }
            .modal-dialog.modal-dialog-centered {
                width: auto !important;
            }
            .modal-body {
                overflow-x: scroll !important;
            }
            .product-details {
                margin: 35px 0px !important;
            }
        }
    </style>

</head>

<body style="
    background-image: url(https://previews.123rf.com/images/swetlanas/swetlanas1908/swetlanas190800728/128424668-vector-seamless-pattern-of-hand-drawn-doodle-sketch-chicken-isolated-on-white-background.jpg);
    background-size: 37%;
    background-attachment: fixed;
    background-position: center;
">
    <!-- HEADER -->
    <header>
        <!-- TOP HEADER -->
        <div id="top-header">
            <div class="container">
                <ul class="header-links pull-left" style="line-height: 25px;">
                    <li>
                        <span style="color: #fff !important; font-weight: bold;">
                            Ikuti Kami
                        </span>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            <i class="fa fa-instagram" style="color: #fff !important; font-weight: bold;"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            <i class="fa fa-twitter" style="color: #fff !important; font-weight: bold;"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            <i class="fa fa-facebook" style="color: #fff !important; font-weight: bold;"></i>
                        </a>
                    </li>
                </ul>
                {{-- <ul class="header-links pull-right" style="
                    padding: 1px 7px;
                    background: #D10024;
                    border-radius: 5px;
                    width: max-content;
                ">
                    @if(session()->has('user'))
                        <li><a style="color:white" href="{{ route('user.history') }}">{{ session()->get('user')->full_name}} </a></li>
                        <li><a href="{{ route('user.logout') }}"><i class="fa fa-user-o"></i> Logout</a></li>
                    @else
                    <li>
                        <a href="{{ route('user.login') }}" style="color: #fff !important;">
                            Login
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('user.signup') }}" style="color: #fff !important;">
                            Daftar
                        </a>
                    </li>
                    @endif
                </ul> --}}
            </div>
        </div>
        <!-- /TOP HEADER -->

        <!-- MAIN HEADER -->
        <div id="header" style="padding: 25px 0px !important;">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- LOGO -->
                    <div class="col-md-3">
                        <div class="header-logo">
                            <a href="{{ route('user.home') }}" class="logo">
                                <img src="{{ asset('img/dapoer-mangan/logo.png') }}" class="img-logo" alt="">
                            </a>
                        </div>
                    </div>
                    <!-- /LOGO -->

                    <!-- SEARCH BAR -->
                    <div class="col-md-6">
                        <div class="header-search">
                            <form action="{{ route('user.search') }}" method="get">
                                <div class="custom_search_top" >
                                    <input class="input" style="outline: none; border-radius: 40px 0px 0px 40px;" name="n" placeholder="Temukan disini..">
                                    <button class="search-btn">
                                        <i class="fa fa-search mx-1"></i> Cari
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /SEARCH BAR -->

                    <!-- ACCOUNT -->
                    <div class="col-md-3 clearfix">
                        <div class="header-ctn" style="
                            justify-content: center;
                            width: 100%;
                            display: flex;
                            align-items: center;
                            position: relative;
                            top: 10px;
                        ">
                            <!-- Cart -->
                            <div class="dropdown" style="
                                /* display: flex !important;
                                position: relative;
                                top: 10px; */
                            ">
                                <a id="custom_shopping_cart" href="{{ route('user.cart') }}" style="
                                    display: flex;
                                    flex-wrap: wrap;
                                ">
                                    <span class="badge badge-danger" style="
                                        background: #D10024;
                                        line-height: normal;
                                        position: relative;
                                        right: -45px;
                                        top: -15px;
                                        z-index: 999;
                                    ">
                                        {{ Session::get('cart') == NULL ? '0' : count(explode(',', Session::get('cart'))) }}
                                    </span>
                                    <i class="fa fa-shopping-cart" style="
                                        font-size: 23px;
                                        position: relative;
                                        top: -2px;
                                        margin: 0px 10px;
                                    "></i>
                                </a>
                            </div>
                            <!-- /Cart -->

                            <div id="user-account">
                                <ul class="header-links pull-right" style="
                                    padding: 5px 7px;
                                    background: #D10024;
                                    border-radius: 5px;
                                    width: max-content;
                                ">
                                    @if(session()->has('user'))
                                        <li style="margin-right: 0px !important;">
                                            <a style="color:white; padding: 0px 10px;" href="{{ route('user.history') }}">
                                                {{ session()->get('user')->full_name}}
                                            </a>
                                        </li>
                                        <li style="margin-right: 0px !important;">
                                            <a id="logout-btn" href="{{ route('user.logout') }}" style="
                                                color: #000;
                                                font-weight: bold;
                                                border-left: 1px solid #000;
                                                padding: 0px 10px;
                                            ">
                                                Logout <i class="fa fa-sign-out" style="color: #000; margin: 0px 2px;"></i>
                                            </a>
                                        </li>
                                    @else
                                    <li>
                                        <a href="{{ route('user.login') }}" style="color: #fff !important;">
                                            Login
                                            <i class="fa fa-sign-in" style="
                                                color: #fff !important;
                                                margin: 0px 3px;
                                            "></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('user.signup') }}" style="color: #fff !important;">
                                            Daftar
                                            <i class="fa fa-user" style="
                                                color: #fff !important;
                                                margin: 0px 3px;
                                            "></i>
                                        </a>
                                    </li>
                                    @endif
                                </ul>
                            </div>

                            <!-- Menu Toogle -->
                            <div class="menu-toggle pull-right" style="margin-right: 0px !important;">
                                <a href="#" style="
                                    width: 35px;
                                    margin-right: 15px;
                                ">
                                    <i class="fa fa-bars" style="
                                        position: relative;
                                        top: -2px;
                                    "></i>
                                    {{-- <span>Menu</span> --}}
                                </a>
                            </div>
                            <!-- /Menu Toogle -->
                        </div>
                    </div>
                    <!-- /ACCOUNT -->
                </div>
                <!-- row -->
            </div>
            <!-- container -->
        </div>
        <!-- /MAIN HEADER -->
    </header>
    <!-- /HEADER -->

    <!-- NAVIGATION -->
    <nav id="navigation">
        <!-- container -->
        <div class="container">
            <!-- responsive-nav -->
            <div id="responsive-nav" style="z-index: 999 !important;">
                <!-- NAV -->
                <ul class="main-nav nav navbar-nav">
                    <li class="{{ Route::is('user.home') ? 'active' : '' }}">
                        <a href="{{ route('user.home') }}">
                            Home
                            <i class="fa fa-cutlery" style="margin: 0px 5px;"></i>
                        </a>
                    </li>
                    <li class="{{ Route::is('user.about-us') ? 'active' : '' }}">
                        <a href="{{ route('user.about-us') }}">
                            Tentang Kami
                            <i class="fa fa-building" style="margin: 0px 5px;"></i>
                        </a>
                    </li>
                    <li class="{{ Route::is('user.contact') ? 'active' : '' }}">
                        <a href="{{ route('user.contact') }}">
                            Kontak
                            <i class="fa fa-phone" style="margin: 0px 5px;"></i>
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Menu Kami
                            <i class="fa fa-sort-down" style="margin: 0px 3px;"></i>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown" style="
                            background: #333 !important;
                        ">
                            <div style="
                                display: grid;
                                padding: 0px 10px;
                            ">
                                @if(Route::is('user.search'))
                                    @foreach($cat as $c)
                                    <a class="dropdown-item {{ $a == -1  ? 'active' : ''}}" style="
                                        width: 100%;
                                        margin: 10px 0px;
                                        color: #fff !important;
                                    "
                                    href="{{ route('user.search.cat',['id'=>$c->id]) }}" >
                                        {{ $c->name}}
                                    </a>
                                    @endforeach
                                @else
                                    @foreach($cat as $c)
                                    <a class="dropdown-item" style="
                                        width: 100%;
                                        margin: 10px 0px;
                                        color: #fff !important;
                                    "
                                    href="{{ route('user.search.cat',['id'=>$c->id]) }}" >
                                        {{ $c->name}}
                                    </a>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </li>
                    @if (session()->get('user'))
                    <li>
                        <a href="{{ route('user.history') }}">
                            Riwayat Pembelian
                            <i class="fa fa-sticky-note" style="margin: 0px 5px;"></i>
                        </a>
                    </li>
                    @endif
                    <li>
                        <a href="https://goo.gl/maps/H7h6Lhz8Qz2FVCc26" target="_blank" style="text-decoration: none;">
                            Jl. Raya Tengah No. 30, Jakarta Timur
                            <i class="fa fa-location-arrow" style="margin: 0px 5px;"></i>
                        </a>
                    </li>
                </ul>
                <!-- /NAV -->
            </div>
            <!-- /responsive-nav -->
        </div>
        <!-- /container -->
    </nav>
    <!-- /NAVIGATION -->

    <!-- SECTION -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            @if(Route::is('user.home'))
            <div class="row">
                <!-- shop -->
                @php
                    $counter=0;
                @endphp
                    @foreach($cat as $c)
                @php
                    $counter++;
                    if($counter==6)
                    break;
                @endphp
                <div class="col-md-4 col-xs-6">
                    <div class="shop" style="
                        border-radius: 10px;
                        box-shadow: 3px 3px 7px 3px #ccc;
                    ">
                        <div class="shop-img" style="max-height: 245px;">
                            <img src="{{ asset($c->thumbnail) }}">
                        </div>
                        <div class="shop-body">
                            <h3>{{ $c->name}}</h3>
                            <a href="search?c={{ $c->id}}" class="cta-btn">
                                Beli Sekarang!
                                <i class="fa fa-arrow-circle-right" style="
                                    position: relative;
                                    left: 3px;
                                "></i>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- /shop -->
                @endforeach
            </div>
            @endif
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- SECTION -->


    @yield('content')

    <!-- /SECTION -->

    <div id="newsletter" class="section" style="background: #fff;">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="newsletter">
                        <p>
                            <strong> Slogan </strong> Bisnis Kami
                        </p>
                        <blockquote style="line-height: 35px;">
                            <i>
                                "Untung untuk mencukupi kebutuhan keluarga dan sisanya untuk investasi amal akhirat. <br>
                                Jujur adalah kunci kesuksesan bisnis".
                            </i>
                        </blockquote>
                        {{-- <p>Sign Up for the <strong>NEWSLETTER</strong></p>
                        <form>
                            <input class="input" type="email" placeholder="Enter Your Email">
                            <button class="newsletter-btn"><i class="fa fa-envelope"></i> Subscribe</button>
                        </form>
                        <ul class="newsletter-follow">
                            <li>
                                <a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-pinterest"></i></a>
                            </li> --}}
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /NEWSLETTER -->

    <!-- FOOTER -->
    <footer id="footer" >
        <!-- top footer -->
        <div class="section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row" >
                    <div class="col-md-3 col-xs-6" >
                        <div class="footer" >
                            <h3 class="footer-title">
                                Dapoer Mangan
                            </h3>
                            <p class="text-justify">
                                Kami merintis usaha kami di akhir tahun 2019, tepatnya 27 Desember 2019. Memulai dengan
                                menu ayam geprek, lalu 3 bulan berjalan kami menambah menu dengan ayam goreng dan rice
                                box. Hingga kini kami sudah memiliki 4 menu untuk makanan berat, 5 menu untuk cemilan dan
                                7 menu untuk minuman.
                            </p>
                        </div>
                    </div>

                    <div class="col-md-3 col-xs-6">
                        <div class="footer">
                            <h3 class="footer-title"> Kontak Kami </h3>
                            <ul class="footer-links">
                                <li>
                                    <a href="https://goo.gl/maps/H7h6Lhz8Qz2FVCc26" target="_blank">
                                        <i class="fa fa-map-marker"></i>
                                        Jl. Raya Tengah No. 30
                                    </a>
                                </li>
                                <li>
                                    <a href="https://wa.me/+6289617341858" target="_blank">
                                        <i class="fa fa-phone"></i>
                                        +62-89617341858
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="clearfix visible-xs"></div>

                    <div class="col-md-3 col-xs-6">
                        <div class="footer">
                            <h3 class="footer-title">
                                Informasi
                            </h3>
                            <ul class="footer-links">
                                <li>
                                    <a href="#"> Tentang Kami </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Kontak
                                    </a>
                                </li>
                                <li>
                                    <a href="#">Privacy Policy</a>
                                </li>
                                <li>
                                    <a href="#">Orders and Returns</a>
                                </li>
                                <li>
                                    <a href="#">Terms & Conditions</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-3 col-xs-6">
                        <div class="footer">
                            <h3 class="footer-title">Service</h3>
                            <ul class="footer-links">
                                <li><a href="#">My Account</a></li>
                                <li><a href="#">View Cart</a></li>
                                <li><a href="#">Wishlist</a></li>
                                <li><a href="#">Track My Order</a></li>
                                <li><a href="#">Help</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /top footer -->

        <!-- bottom footer -->
        <div id="bottom-footer" class="section">
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-md-12 text-center">
                        <ul class="footer-payments">
                            <li><a href="#"><i class="fa fa-cc-visa"></i></a></li>
                            <li><a href="#"><i class="fa fa-credit-card"></i></a></li>
                            <li><a href="#"><i class="fa fa-cc-paypal"></i></a></li>
                            <li><a href="#"><i class="fa fa-cc-mastercard"></i></a></li>
                            <li><a href="#"><i class="fa fa-cc-discover"></i></a></li>
                            <li><a href="#"><i class="fa fa-cc-amex"></i></a></li>
                        </ul>
                    </div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /bottom footer -->
    </footer>
    <!-- /FOOTER -->


    <!-- jQuery Plugins -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/slick.min.js') }}"></script>
    <script src="{{ asset('js/nouislider.min.js') }}"></script>
    <script src="{{ asset('js/jquery.zoom.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/lib/jquery.js') }}"></script>
    <script src="{{ asset('js/dist/jquery.validate.js') }}"></script>
</body>

</html>
