@extends('store.storeLayout')
@section('content')
<div class="section">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row" style="margin-bottom: 100px;">

            <!-- STORE -->
            <div id="store" class="col-md-12">
                <!-- store products -->
                <div class="row">
                    @foreach($products as $product)
                    <!-- product -->
                    <a href="view/{{$product->id}}">
                        <div class="col-md-4 col-xs-6">
                            <div class="product" style="
                                box-shadow: 3px 3px 7px 3px #ccc;
                                border-radius: 10px;
                                margin-bottom: 50px !important;
                            ">
                                <div class="product-img">
                                    <img src="uploads/products/{{$product->id}}/{{$product->image_name}}" alt="" style="
                                        border-radius: 10px;
                                        min-height: 360px;
                                    ">
                                    <div class="product-label">
                                        {{-- <span class="sale">offer</span> --}}
                                        {{-- <span class="new">{{$product->tag}}</span> --}}
                                    </div>
                                </div>
                                <div class="product-body" style="
                                    border-radius: 10px;
                                ">
                                    <h3 class="product-name">
                                        {{$product->name}}
                                    </h3>
                                    <h4 class="product-price">
                                        Rp {{ number_format($product->discount,0,',','.') }}
                                        <del class="product-old-price">
                                            Rp {{ number_format($product->price,0,',','.') }}
                                        </del>
                                    </h4>
                                    <div class="product-rating">
                                        <i class="fa fa-star" style="color: gold;"></i>
                                        <i class="fa fa-star" style="color: gold;"></i>
                                        <i class="fa fa-star" style="color: gold;"></i>
                                        <i class="fa fa-star" style="color: gold;"></i>
                                        <i class="fa fa-star" style="color: gold;"></i>
                                    </div>

                                </div>
                                <div class="add-to-cart" style="
                                    border-radius: 10px;
                                ">
                                    <a class="add-to-cart-btn" href="{{route('user.view',['id'=>$product->id])}}">
                                        <i class="fa fa-shopping-cart" style="
                                            top: -10px; left: 15px;
                                        "></i> Lihat Produk
                                    </a>
                                </div>
                            </div>
                        </div>
                    </a>
                    <!-- /product -->
                    @endforeach
                </div>
                <!-- /STORE -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>

    @endsection
