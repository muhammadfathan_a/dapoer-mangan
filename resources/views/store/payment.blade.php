<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>
        {{ ucwords(str_replace('-', ' ', Request::segment(1))) }} | Dapoer Mangan
    </title>

    <link rel="shortcut icon" href="{{ asset('img/dapoer-mangan/logo.png') }}" />

    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" />

    <!-- Slick -->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/slick.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('css/slick-theme.css') }}" />

    <!-- nouislider -->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/nouislider.min.css') }}" />

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/style2.css') }}" />

    <style>
        th.text-center {
            padding: 20px 40px !important;
        }
        @import url(https://fonts.googleapis.com/css?family=Open+Sans:400,300,600);
        html, body {
            margin: 0;
            padding: 0;
            font-family: 'Open Sans';
        }

        h1 {
            color: #fff;
            text-align: center;
            font-weight: 300;
        }

        #slider {
            position: relative;
            overflow: hidden;
            margin: 20px auto 0 auto;
            border-radius: 4px;
        }

        #slider ul {
            position: relative;
            margin: 0;
            padding: 0;
            height: 200px;
            list-style: none;
        }

        #slider ul li {
            position: relative;
            display: block;
            float: left;
            margin: 0;
            padding: 0;
            width: 500px;
            /* height: 300px; */
            background: #ccc;
            text-align: center;
            line-height: 30px;
        }

        a.control_prev, a.control_next {
            position: absolute;
            top: 40%;
            z-index: 999;
            display: block;
            padding: 4% 3%;
            width: auto;
            height: auto;
            background: #fff;
            color: #333;
            text-decoration: none;
            font-weight: 900;
            font-size: 18px;
            opacity: 0.8;
            cursor: pointer;
        }

        a.control_prev:hover, a.control_next:hover {
            opacity: 1;
            -webkit-transition: all 0.2s ease;
        }

        a.control_prev {
            border-radius: 0 2px 2px 0;
        }

        a.control_next {
            right: 0;
            border-radius: 2px 0 0 2px;
        }

        .slider_option {
            position: relative;
            margin: 10px auto;
            width: 160px;
            font-size: 18px;
        }
        .img-scan {
            width: 50%;
            box-shadow: 0px 0px 15px -6px #333;
            border-radius: 5px;
        }
        @media only screen and (max-width: 767px){
            #head_links {
                visibility: hidden;
            }
            .custom_search_top {
                text-align:center;
            }

            .header-ctn {
                width: 100%;
            }
            .pull-left, .pull-right {
                text-align: center !important;
                margin: 10px auto;
            }
            .img-logo {
                margin: 25px 0px;
            }
            .nav .open>a, .nav .open>a:focus, .nav .open>a:hover {
                background-color: transparent !important;
                border-color: transparent !important;
            }
            .header-search form .input {
                width: calc(100% - 145px);
            }
            #user-account {
                display: contents !important;
            }
            #logout-btn {
                color: #000;
                font-weight: bold;
                border: none !important;
            }
            .container {
                padding-right: 25px !important;
                padding-left: 25px !important;
            }
            .modal-dialog.modal-dialog-centered {
                width: auto !important;
            }
            .modal-body {
                overflow-x: scroll !important;
            }
            .product-details {
                margin: 35px 0px !important;
            }
            #box-outer {
                zoom: 70% !important;
            }
        }
    </style>
</head>
<div class="section">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center" style="margin-bottom: 50px;">
                    {{-- <h3 class="title">
                        Lakukan Pembayaran
                    </h3> --}}
                </div>
                <div id="box-outer" style="
                    margin: auto;
                    display: flex;
                    flex-wrap: wrap;
                    justify-content: space-around;
                ">
                    <div class="card" style="
                        box-shadow: 0px 0px 15px 1px #eee;
                        padding: 35px 25px;
                        border-radius: 5px;
                    ">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="row max-auto" style="
                                        align-items: center;
                                    ">
                                        <div class="col-sm-12 col-md-12" style="margin: 20px auto; padding: 0px;">
                                            <h3 style="
                                                font-weight: 500;
                                                text-align: center;
                                                margin-bottom: 0px;
                                                font-weight: 800;
                                            ">
                                                Scan Barcode
                                            </h3>
                                        </div>
                                    </div>
                                    <hr>
                                    <center class="text-danger">
                                        Geser untuk mengganti metode pembayaran
                                    </center>
                                    <div id="slider" style="margin: 50px 0px;">
                                        <a class="control_next">
                                            <i class="fa fa-arrow-right"></i>
                                        </a>
                                        <a class="control_prev">
                                            <i class="fa fa-arrow-left"></i>
                                        </a>
                                        <ul>
                                            <li style="background: #fff;">
                                                <img class="img-scan"
                                                src="{{ asset('img/payment/gopay.jpeg') }}">
                                                <br><br><h4><b> GOPAY </b></h4>
                                                <span style="font-size: 19px;">
                                                    082211233120. Ahmad Ali
                                                </span>
                                            </li>
                                            <li style="background: #fff;">
                                                <img class="img-scan"
                                                src="{{ asset('img/payment/ovo.jpeg') }}">
                                                <br><br><h4><b> OVO </b></h4>
                                                <span style="font-size: 19px;">
                                                    082211233120. Ahmad Ali
                                                </span>
                                            </li>
                                            <li style="background: #fff;">
                                                <img class="img-scan"
                                                src="{{ asset('img/payment/shopee.jpeg') }}">
                                                <br><br><h4><b> SHOPEE PAY </b></h4>
                                                <span style="font-size: 19px;">
                                                    082211233120. Ahmad Ali
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="price-total text-center" style="line-height: 45px;">
                                        <span>
                                            harga yang harus dibayar:
                                        </span>
                                        <h3 style="font-weight: 600;">
                                            Rp {{ number_format($sale->first()->price + 10000,0,',','.') }}
                                        </h3>
                                    </div>
                                    <br>
                                    <br><br><br>
                                    <h3 style="font-weight: 900; text-align: center;">
                                        Transfer via Bank
                                    </h3>
                                    <hr>
                                    <ul>
                                        <li style="text-align: center;">
                                            <span style="font-size: 19px;">
                                                044201019788504/BRI. Ahmad Ali
                                            </span>
                                        </li>
                                    </ul>
                                    <hr>
                                    <div style="margin-top: 40px; padding: 0px; width: 100%; text-align: center;">
                                        <a href="https://wa.me/+6289617341858?text=Permisi,%20saya%20ingin%20konfirmasi%20pembayaran....%20"
                                        class="btn btn-success" target="_blank" style="
                                            font-weight: 600; width: max-content;
                                        ">
                                            <i class="fa fa-whatsapp mx-1"></i>
                                            Konfirmasi via WhatsApp
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Billing Details -->
    </div>
</div>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/slick.min.js') }}"></script>
<script src="{{ asset('js/nouislider.min.js') }}"></script>
<script src="{{ asset('js/jquery.zoom.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script src="{{ asset('js/lib/jquery.js') }}"></script>
<script src="{{ asset('js/dist/jquery.validate.js') }}"></script>
<script>
    jQuery(document).ready(function ($) {
        $('#checkbox').change(function(){
        setInterval(function () {
            moveRight();
        }, 3000);
        });

        var slideCount = $('#slider ul li').length;
        var slideWidth = $('#slider ul li').width();
        var slideHeight = $('#slider ul li').height();
        var sliderUlWidth = slideCount * slideWidth;

        $('#slider').css({ width: slideWidth, height: slideHeight });

        $('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });

        $('#slider ul li:last-child').prependTo('#slider ul');

        function moveLeft() {
            $('#slider ul').animate({
                left: + slideWidth
            }, 200, function () {
                $('#slider ul li:last-child').prependTo('#slider ul');
                $('#slider ul').css('left', '');
            });
        };

        function moveRight() {
            $('#slider ul').animate({
                left: - slideWidth
            }, 200, function () {
                $('#slider ul li:first-child').appendTo('#slider ul');
                $('#slider ul').css('left', '');
            });
        };

        $('a.control_prev').click(function () {
            moveLeft();
        });

        $('a.control_next').click(function () {
            moveRight();
        });
    });
</script>
