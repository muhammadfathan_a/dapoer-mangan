@extends('store.storeLayout')
@section('content')
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">

                <!-- section title -->
                <div class="col-md-12">
                    <div class="table-responsive mb-4" style="
                        box-shadow: 0px 0px 15px -5px #333;
                        border-radius: 20px;
                    ">
                        <table style="
                            background:#fff; width:100%;height: 100%;
                        " cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="padding: 20px;">
                                        <table style="width: auto;height: 100%;margin: 0 auto" cellpadding="0" cellspacing="0" border="0">
                                            <tbody>
                                            <tr style="border-bottom:1px dashed #ddd;">
                                                <td style="padding-top: 20px; align-items: center; padding: 10px 0px;">
                                                    {{-- <img style="float:left;" src="{{ asset('img/dapoer-mangan/logo.png') }}" alt="Logo"> --}}
                                                    <h2>
                                                        Dapoer Mangan
                                                        <p style="font-family: Roboto;font-size: 13px;font-weight: 500;font-style:
                                                        normal;font-stretch: normal;line-height: normal;letter-spacing: normal;
                                                        color:#333;margin-top: 10px;">
                                                            Cara asik solusi lapar dan dahaga anda.
                                                        </p>
                                                    </h2>
                                                </td>
                                            </tr>
                                            {{-- <tr>
                                                <td style="padding-top: 20px; text-align: center;">
                                                    <img style="max-width: 100%;width:30%;border-radius: 10px;"
                                                    src="{{ asset('images/faces/face1.jpg') }}" alt="email image">
                                                </td>
                                            </tr> --}}
                                            <tr>
                                                <td style="border-radius: 10px;background: #fff;padding: 30px 30px 20px 30px;margin-top: 20px;display: block;">
                                                    <p class="text-danger" style="font-family: Roboto;font-size: 21px;font-weight: 500;font-style: normal;font-stretch: normal;line-height: 1.11;letter-spacing: normal;">
                                                        Taufik Imam Santoso <br>
                                                        <small class="text-muted" style="font-size: 14px;">
                                                            Owner
                                                        </small>
                                                    </p>
                                                    <br><br>
                                                    <blockquote class="text-justify" style="font-family: Roboto;font-size: 17px;font-weight: normal;font-style:
                                                    normal;font-stretch: normal;line-height: 1.71;letter-spacing: normal;color: #001737;">
                                                        <q>Kami merintis usaha kami di akhir tahun 2019, tepatnya 27 Desember 2019.
                                                            Memulai dengan menu ayam geprek, lalu 3 bulan berjalan kami menambah menu
                                                            dengan ayam goreng dan rice box. Hingga kini kami sudah memiliki 4 menu
                                                            untuk makanan berat, 5 menu untuk cemilan dan 7 menu untuk minuman.</q>
                                                    </blockquote>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
    </div>
@endsection
