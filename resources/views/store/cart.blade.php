@extends('store.storeLayout')
@section('content')
<script src="{{ asset('js/lib/jquery.js') }}"></script>
<script src="{{ asset('js/dist/jquery.validate.js') }}"></script>

<link type="text/css" rel="stylesheet" href="{{ asset('css/style_for_quantity.css') }}" />
<style>
    label.error {
        color: #a94442;
        background-color: #f2dede;
        border-color: #ebccd1;
        padding:1px 20px 1px 20px;
    }
    .rTable {
        display: block;
        width:100%;
    }
    .rTableHeading, .rTableBody, .rTableFoot, .rTableRow{
        clear: both;
    }
    .rTableHead, .rTableFoot{
        background-color: #DDD;
        font-weight: bold;
    }
    .rTableHead {
        padding: 25px 1.8%;
        margin-bottom: 10px;
    }
    .rTableCell, .rTableHead {
        float: left;
        overflow: hidden;
        padding: 20px 1.8%;
        width:20%;
    }
    .rTable:after {
        visibility: hidden;
        display: block;
        font-size: 0;
        content: " ";
        clear: both;
        height: 0;
    }
</style>

<!-- SECTION -->
<div class="section">
    <!-- container -->
    <div class="container">
        <!-- row -->
            <!-- Order Details -->
            <div class="col-md-5 order-details" style="width: 100%;">
                <div class="section-title text-center">
                    <h3 class="title">
                        Pesanan Anda
                    </h3>
                </div>
                <div id="order_summary" class="order-summary">
                @if($all != null)
                <div class="rTable">
                    <div class="rTableRow">
                        <div class="rTableHead">
                            <strong> Hapus </strong>
                        </div>
                        <div class="rTableHead">
                            <strong> Nama Produk </strong>
                        </div>
                        <div class="rTableHead">
                            <strong> Jumlah </strong>
                        </div>
                        <div class="rTableHead"><strong> Varian </strong></div>
                        <div class="rTableHead"><strong> Harga </strong></div>
                    </div>
					@foreach($all as $c)
                        @foreach($prod as $p)
                            @if($c[0]==$p->id)
                                <div class="rTableRow" id="deleteItem_{{ $c[3] }}">
                                    <div class="rTableCell">
                                        <button type="button" id="delete_item" value={{ $c[3] }}
                                        name="delete_item" class="delete_item">
                                            X
                                        </button>
                                    </div>
                                    <div class="rTableCell">
                                        <img src="uploads/products/{{ $p->id }}/{{ $p->image_name }}"
                                        height="50px" width="50px">
                                        {{ $p->name }}
                                    </div>
                                    <div class="rTableCell">
                                        <button type="button" id="sub" value={{ $p->id }} data-rel={{ $c[3] }}
                                        data-rel2={{ number_format($p->discount,0,',','.') }} class="sub">
                                            -
                                        </button>
                                        <input class="text-center" type="number" id="quantity" style="width:auto;"
                                        name={{ $p->id }} value={{ $c[1] }} min="1" max="100" readonly/>
                                        <button type="button" id="add" value={{ $p->id }} data-rel={{ $c[3] }}
                                        data-rel2={{ number_format($p->discount,0,',','.') }}  class="add">
                                            +
                                        </button>
                                    </div>
                                    <div class="rTableCell">
                                        {{  $c[2]  }}
                                    </div>
                                    <div class="rTableCell">
                                        <div id="individualPrice_{{ $c[3] }}">
                                            Rp
                                            @php
                                                $tot =$p->discount* $c[1];
                                                echo number_format($tot,0,',','.');
                                            @endphp
                                        </div>
                                    </div>
                                </div>
                            @break
                        @endif
                    @endforeach
                @endforeach
                </div>
                <div class="order-col">
                    <div>Ongkir</div>
                    <div><strong>Rp 10.000 </strong></div>
                </div>
                <div class="order-col">
                    <div><strong>TOTAL</strong></div>
                    <div>
                        <strong class="order-total" id="totalCost">
                            Rp {{ number_format(Session::get('price') + 10000,0,',','.') }}
                        </strong>
                    </div>
                </div>
                @else
                <div class="order-col" style="margin-top: 125px;">
                    <h1 style="font-weight: 500;">
                        Keranjang Kosong
                    </h1>
                </div>
                @endif

            </div>
            <div class="payment-method">
                <div class="input-radio">
                    <input type="radio" name="payment" id="payment-2" checked>
                    <div class="caption">
                        <p>
                            Produk akan dikirim dalam waktu 24 jam setelah konfirmasi.
                            Kami hanya menerima Pembayaran Non-Tunai saat ini.
                        </p>
                    </div>
                </div>
            </div>
            @if(session('user'))
                @if($all != null)
                <center>
                    <form method="post" name="cart">
                        {{ csrf_field() }}
                        <input type="submit" id="confirm_order"  name="order" class="primary-btn order-submit" value="Konfirmasi Pesanan">
                    </form>
                </center>
                @else
                    <a href="{{ route('user.home') }}">
                        <input type="button"  class="primary-btn order-submit" value="Pesan Sekarang">
                    </a>
                @endif
            @elseif(!session('user'))
            <div class="row" style="margin-top: 115px !important;">
                <div id="signupForm">
                    <div class="col-md-7">
                        <!-- Billing Details -->
                        <div class="billing-details">
                            <div class="section-title">
                                <h3 class="title">
                                    <span style="font-weight: 100; text-transform: lowercase !important; font-size: 17px;">
                                        Silakan
                                    </span>
                                    <a href="{{  route('user.login')  }}" class="text-danger"> Login </a>
                                    <span style="font-weight: 100; text-transform: lowercase !important; font-size: 17px;">
                                        atau
                                    </span>
                                    <a href="{{  route('user.signup')  }}" class="text-danger"> Daftar </a>
                                    <span style="font-weight: 100; text-transform: lowercase !important; font-size: 17px;">
                                        Terlebih Dahulu
                                    </span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <!-- /Order Details -->
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</div>
<!-- /SECTION -->

<script>

   //TO DO: ajax will take place

    $('.add').click(function () {

    var url="{{ route('user.editCart') }}";
    var product_id= $(this).val();
    $(this).prev().val(+$(this).prev().val() + 1);
    var x=$(this).prev().val();
    var token=$("input[name=_token]").val();
    var order_serial=this.getAttribute('data-rel');
    var product_price=this.getAttribute('data-rel2');


    $.ajax({
            type:'post',
            url:url,
            dataType: "JSON",
            async: false,
            data:{pid: product_id, newQ:x, oSerial:order_serial, _token: token},
            success:function(msg)
            {
                document.getElementById("individualPrice_"+order_serial).innerHTML="Rp "+x*product_price;
                document.getElementById("totalCost").innerHTML = "Rp " + msg[2];
            }
            });


    });
    $('.sub').click(function () {

        var url="{{ route('user.editCart') }}";
        var product_id= $(this).val();
        var order_serial=this.getAttribute('data-rel');
        var product_price=this.getAttribute('data-rel2');
        if ($(this).next().val() > 1)
        {
            $(this).next().val(+$(this).next().val() - 1);
            var x=$(this).next().val();
            var token=$("input[name=_token]").val();


            $.ajax({
            type:'post',
            url:url,
            dataType: "JSON",
            async: false,
            data:{pid: product_id, newQ:x, oSerial:order_serial, _token: token},
            success:function(msg)
            {
                document.getElementById("individualPrice_"+order_serial).innerHTML="Rp " + x*product_price;
                document.getElementById("totalCost").innerHTML = "Rp " + msg[2];

            }
            });


        }
    });

    $('.delete_item').click(function () {
        var url="{{ route('user.deleteCartItem') }}";
        var serial= $(this).val();   //serial is the forth element of sale coloumn
        var token=$("input[name=_token]").val();
        var id_holder="deleteItem_"+serial;
        $.ajax({
                type:'post',
                url:url,
                dataType: "JSON",
                async: false,
                data:{serial:serial, _token: token},
                success:function(msg)
                {
                    if(msg=="Empty")
                        {
                        document.getElementById("order_summary").innerHTML = "<div class='order-col'><h1> Keranjang Kosong </h1></div>";
                        document.getElementById("confirm_order").style.visibility = "hidden";
                        }

                    //$("#deleteItem_".$p->id").load(location.href+" #refresh_div","");
                    document.getElementById(id_holder).innerHTML  = "";
                    document.getElementById("totalCost").innerHTML = msg[2];
                }
                });


    });


    //validation

    $(document).ready(function() {
		// validate the comment form when it is submitted
		//$("#commentForm").validate();

		// validate signup form on keyup and submit
		$("#signupForm").validate({
			rules: {
				name: "required",
				email: {
					required: true,
					email: true
				},
                address: "required",
                city: "required",
                zip: {
					required: true,
					number: true
				},
                tel: "required",
				pass: {
					required: true,
					minlength: 5
				},
				confirm_password: {
					required: true,
					minlength: 5,
					equalTo: "#pass"
				}



			},
			messages: {
				name: "Please enter your Fullname",
				email: "Please enter a valid email address",
                address: "Please enter your Address",
                city: "Please enter your City",
                address: "Please enter your Address",
				zip: {
					required: "Please enter Zipcode",
					number: "Invalid Zipcode"
				},
                tel: "Please enter your Phone number",
				pass: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long"
				},
				confirm_password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long",
					equalTo: "Please enter the same password as above"
				}


			}



		});


	});

</script>
<script>
function myFunction() {
    //var token={{  csrf_token()  }};
    var email=$("#email").val();
    var token=$("input[name=_token]").val();
    var url="{{ route('user.signup.check_email') }}";


            $.ajax({
                type:'post',
                url:url,
                dataType: "JSON",
                async: false,
                data:{email: email, _token: token},
                success:function(msg){


                        if(msg == "1")
                            {
                                document.getElementById("for_duplicate-email").innerHTML = "<label class='error'>This Email Address is Already taken</label>";


                            }
                    else
                        {
                            document.getElementById("for_duplicate-email").innerHTML = "";

                        }
                    }
             });

}
</script>
@endsection
