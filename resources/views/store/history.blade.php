@extends('store.storeLayout')
    <style>
        th.text-center {
            padding: 20px 40px !important;
        }
    </style>
    @section('content')
    <!-- SECTION -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center" style="margin-bottom: 70px;">
                        <h3 class="title">
                            Riwayat Pemesanan
                        </h3>
                    </div>
                    <!-- Button trigger modal -->
                    @foreach($sale as $s)
                        @foreach($all as $c)
                            @if($c[0]==$s->id)
                                @foreach($products as $p)
                                    @if(session('user')->id == $s->user_id)
                                        @if($c[1]==$p->id)
                                            @php
                                                $index[] = $s->id;
                                                $unique = array_unique($index);
                                                $number = count(array_count_values($index));
                                            @endphp
                                        @break
                                        @endif
                                    @endif
                                @endforeach
                            @endif
                        @endforeach
                    @endforeach
                    <div style="
                        margin: auto;
                        display: flex;
                        flex-wrap: wrap;
                        justify-content: space-around;
                    ">
                        @if (isset($unique))
                            <div class="row mx-auto" style="width: 100%;">
                            @foreach ($unique as $id)
                                <div class="col-sm-12 col-md-3" style="
                                    line-height: 25px;
                                    margin: 20px 30px;
                                    background: #fff;
                                    padding: 15px 20px;
                                    border-radius: 10px;
                                    box-shadow: 0px 0px 15px -5px #333;
                                ">
                                    <b>
                                        INV.{{
                                            session()->get('user')->id.'/'.
                                            Carbon\Carbon::parse(session()->get('user')->created_at)->format('y').
                                            '/00'.$id
                                        }}
                                    </b>
                                    <br>
                                    @foreach ($sale as $apaan)
                                        @if ($id == $apaan->id)
                                            Status Pesanan: <b style="text-transform: capitalize;">
                                                {{ $apaan->order_status }}
                                            </b>
                                            <br>
                                            <small>
                                                Tgl Pemesanan: {{ Carbon\Carbon::parse($apaan->created_at) }}
                                            </small>
                                            @php
                                                if ($apaan->order_status == 'ditempatkan') { $color = 'btn-primary'; }
                                                else if($apaan->order_status == 'diproses') { $color = 'btn-warning'; }
                                                else if($apaan->order_status == 'diterima') { $color = 'btn-success'; }
                                                else if($apaan->order_status == 'batal') { $color = 'btn-danger'; }
                                            @endphp
                                            <button type="button" class="btn {{ $color }}" data-toggle="modal"
                                            data-target="#historyModal-{{ $id }}" style="margin: 10px 0px;">
                                                {{ $apaan->order_status != 'ditempatkan' ? 'Lihat Pesanan' : 'Lihat/Bayar Pesanan' }}
                                            </button>
                                            <!-- Modal -->
                                            <div class="modal fade" id="historyModal-{{ $id }}" tabindex="-1"
                                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" style="
                                                    max-width: none;
                                                    width: max-content;
                                                ">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <div style="display: flex; justify-content: space-between;">
                                                                <h5 class="modal-title" id="exampleModalLabel">
                                                                    <span style="font-weight: normal;">No. Transaksi</span>
                                                                    INV.{{
                                                                        session()->get('user')->id.'/'.
                                                                        Carbon\Carbon::parse(session()->get('user')->created_at)->format('y').
                                                                        '/00'.$id
                                                                    }}
                                                                </h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="modal-body">
                                                            <table class="table table-striped table-borderless" style="margin-bottom: 0px;">
                                                                <thead class="bg-white">
                                                                    <th class="text-center"> Gambar </th>
                                                                    <th class="text-center"> Nama </th>
                                                                    <th class="text-center"> Varian </th>
                                                                    <th class="text-center"> Jumlah </th>
                                                                    <th class="text-center"> Harga Satuan </th>
                                                                    <th class="text-center"> Harga Total </th>
                                                                </thead>
                                                                <tbody>
                                                                @foreach ($sale as $item)
                                                                    @php $getSale[] = $item->id; $beda = array_unique($getSale); @endphp
                                                                @endforeach
                                                                @foreach ($beda as $nice)
                                                                    @foreach($sale as $s)
                                                                        @if ($nice == $s->id)
                                                                            @foreach($all as $c)
                                                                                @if($c[0]==$s->id)
                                                                                    @foreach($products as $p)
                                                                                        @if(session('user')->id == $s->user_id)
                                                                                            @if($c[1]==$p->id)
                                                                                                @if ($s->id == $id)
                                                                                                    <tr>
                                                                                                        <td class="text-center" align="center">
                                                                                                            <img src="{{ asset('uploads/products/'.$p->id.'/'.$p->image_name)  }}" height="120px" width="120px"
                                                                                                            style="border-radius: 5px;">
                                                                                                        </td>
                                                                                                        <td class="text-center" style="vertical-align: middle;">
                                                                                                            {{ $p->name }}
                                                                                                        </td>
                                                                                                        <td class="text-center" style="vertical-align: middle;">
                                                                                                            {{ $c[3] }}
                                                                                                        </td>
                                                                                                        <td class="text-center" style="vertical-align: middle;">
                                                                                                            {{ $c[2] }}
                                                                                                        </td>
                                                                                                        <td class="text-center" style="vertical-align: middle;">
                                                                                                            Rp {{ number_format($p->discount,0,',','.') }}
                                                                                                        </td>
                                                                                                        @php
                                                                                                            $priceSum = $p->discount * $c[2];
                                                                                                        @endphp
                                                                                                        <td class="text-center" style="vertical-align: middle;">
                                                                                                            Rp {{ number_format($priceSum,0,',','.') }}
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    @php $orderStatus = $s->order_status; @endphp
                                                                                                @endif
                                                                                            @break
                                                                                            @endif
                                                                                        @endif
                                                                                    @endforeach
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                @endforeach
                                                                @foreach ($sale as $saleWoy)
                                                                    @if ($saleWoy->id == $id)
                                                                        <tr id="{{ $saleWoy->id }}">
                                                                            <th colspan="5" class="text-left" style="vertical-align: middle;">
                                                                                Ongkir
                                                                            </th>
                                                                            <th class="text-center" style="vertical-align: middle;">
                                                                                Rp 10.000
                                                                            </th>
                                                                        </tr>
                                                                        <tr id="{{ $saleWoy->id }}">
                                                                            <th colspan="5" class="text-left" style="vertical-align: middle;">
                                                                                Harga Total Pembayaran
                                                                            </th>
                                                                            <th class="text-center" style="vertical-align: middle;">
                                                                                Rp {{ number_format($saleWoy->price + 10000,0,',','.') }}
                                                                            </th>
                                                                        </tr>
                                                                    @endif
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <div class="row">
                                                                <div class="col-sm-12 col-md-6">
                                                                    <span style="
                                                                    float: left;
                                                                    /* position: relative;
                                                                    top: 5px; */
                                                                        ">
                                                                        Status Pemesanan
                                                                        <b style="text-transform: capitalize;">{{ $orderStatus }}</b>
                                                                    </span>
                                                                </div>
                                                                <div class="col-sm-12 col-md-6">
                                                                    <a class="btn btn-success" target="_blank"
                                                                    style="display: {{ $orderStatus != 'ditempatkan' ? 'none' : '' }};"
                                                                    href="{{ route('user.payment', ['id' => $id]) }}">
                                                                        Bayar Sekarang
                                                                        <i class="fa fa-money" style="margin: 0px 5px;"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            @endforeach
                            </div>
                        @else
                        <p style="margin-top: 15px;">
                            Belum ada riwayat pemesanan Anda.
                        </p>
                        @endif
                    </div>
                </div>
            </div>
            <!-- /Billing Details -->
        </div>
    </div>
@endsection
