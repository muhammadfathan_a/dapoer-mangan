@extends('store.storeLayout')
@section('content')
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">

                <!-- section title -->
                <div class="col-md-12">
                    <div class="section-title">
                        <h3 class="title"> Produk Terbaru </h3>
                    </div>
                </div>
                <!-- /section title -->

                <!-- Products tab & slick -->
                <div class="col-md-12">
                    <div class="row">
                        @forelse($products as $product)
                        <!-- product -->
                        <a href="{{route('user.view',['id'=>$product->id])}}">
                            <div class="col-md-3">
                                <div class="product" style="
                                    box-shadow: 3px 3px 7px 3px #ccc;
                                    border-radius: 10px;
                                    margin-bottom: 50px !important;
                                ">
                                    <div class="product-img">
                                        <img src="uploads/products/{{$product->id}}/{{$product->image_name}}" alt="" style="
                                            border-radius: 10px;
                                            min-height: 263px;
                                        ">
                                    </div>
                                    <div class="product-body" style="
                                        border-radius: 10px;
                                    ">
                                        <p class="product-category">
                                            {{$product->category->name}}
                                        </p>
                                        <h3 class="product-name">
                                            {{$product->name}}
                                        </h3>
                                        <h4 class="product-price">
                                            Rp {{number_format($product->discount,0,',','.')}}
                                            <del class="product-old-price">
                                                Rp {{ number_format($product->price,0,',','.') }}
                                            </del>
                                        </h4>
                                        <div class="product-rating">
                                            <i class="fa fa-star" style="color: gold;"></i>
                                            <i class="fa fa-star" style="color: gold;"></i>
                                            <i class="fa fa-star" style="color: gold;"></i>
                                            <i class="fa fa-star" style="color: gold;"></i>
                                            <i class="fa fa-star" style="color: gold;"></i>
                                        </div>
                                    </div>
                                    <div class="add-to-cart" style="
                                        border-radius: 10px;
                                    ">
                                        <a class="add-to-cart-btn" href="{{route('user.view',['id'=>$product->id])}}">
                                            <i class="fa fa-shopping-cart" style="
                                                top: -10px; left: 15px;
                                            "></i> Lihat Produk
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <!-- /product -->
                        @empty
                        <div class="col-md-12 justify-content-center" style="margin-top: 45px !important;">
                            <span>
                                "Nantikan produk terbaru dari kami!
                                <span style="font-size: 19px !important;"> 😊 </span>
                            </span>
                        </div>
                        @endforelse
                    </div>

                </div>
                <div class="col-sm-12">
                    {{ $products->links() }}
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
    </div>
@endsection
