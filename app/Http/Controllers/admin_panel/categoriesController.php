<?php

namespace App\Http\Controllers\admin_panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryVerifyRequest;
use App\Http\Requests\CategoryEditVerifyRequest;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\Category;
use App\Product;



class categoriesController extends Controller
{
    public function index()
    {
    	return view('admin_panel.categories.index');

    }

    public function posted(CategoryVerifyRequest $request)
    {
        // file management
        $filenameWithExt    =   str_replace(" ", "_", $request->file('inp_files')->getClientOriginalName());
        $filename           =   pathinfo($filenameWithExt, PATHINFO_FILENAME);
        $extension          =   $request->file('inp_files')->getClientOriginalExtension();
        $fileNameToStore    =   str_replace(" ", "-", $filename).'_'.'.'.$extension;

        $cat = new Category();
        $cat->name = $request->Name;
        $cat->type = $request->Type;
        $cat->thumbnail = $request->file('inp_files')->move('img/kategori/', $fileNameToStore);
        $cat->save();

        return redirect()->route('admin.categories');
    }

    public function edit($id)
    {
        $cat = Category::find($id);

        return view('admin_panel.categories.edit')
            ->with('category', $cat);
    }

    public function update(CategoryEditVerifyRequest $request, $id)
    {
        try
        {
            $catToUpdate = Category::find($request->id);
            $catToUpdate->name = $request->Name;
            $catToUpdate->type = $request->Type;

            if ($request->inp_files != NULL)
            {
                $file_path = base_path('public/'.$request->del_img);
                // app_path("public/test.txt");

                if(File::exists($file_path))
                {
                    File::delete($file_path);

                    // file management
                    $filenameWithExt    =   str_replace(" ", "_", $request->file('inp_files')->getClientOriginalName());
                    $filename           =   pathinfo($filenameWithExt, PATHINFO_FILENAME);
                    $extension          =   $request->file('inp_files')->getClientOriginalExtension();
                    $fileNameToStore    =   str_replace(" ", "-", $filename).'_'.'.'.$extension;

                    $catToUpdate->thumbnail = $request->file('inp_files')->move('img/kategori/', $fileNameToStore);
                }
            }
            $catToUpdate->save();

            return redirect()->route('admin.categories');
            // return base_path('public/'.$request->del_img);

        } catch (\Exception $e)
        {
            return $e->getMessage();
        }
    }

    public function delete($id)
    {

        $cat = Category::find($id);

        return view('admin_panel.categories.delete')
            ->with('category', $cat);
    }

    public function destroy(Request $request)
    {
        $file_path = base_path('public/'.$request->del_img);
        // app_path("public/test.txt");

        if(File::exists($file_path))
        {
            File::delete($file_path);

            //Deleting Category related Products
            $prdsToDelete = Product::all()->where('category_id', $request->id);

            foreach ($prdsToDelete as $prdToDelete)
            {
                //deleting image folder
                try{
                    $src='uploads/products/'.$prdToDelete->id.'/';
                    $dir = opendir($src);
                    while(false !== ( $file = readdir($dir)) ) {
                        if (( $file != '.' ) && ( $file != '..' )) {
                            $full = $src . '/' . $file;
                            if ( is_dir($full) ) {
                                rrmdir($full);
                            }
                            else {
                                unlink($full);
                            }
                        }
                    }
                    closedir($dir);
                    rmdir($src);
                }
                catch(\Exception $e)
                {
                    return $e->getMessage();
                }
                //deleting image folder done
                $prdToDelete->delete();
            }

            $catToDelete = Category::find($request->id);
            $catToDelete->delete();

            return redirect()->route('admin.categories');
        }
    }
}
