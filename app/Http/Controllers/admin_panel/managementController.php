<?php

namespace App\Http\Controllers\admin_panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Input;
use App\Product;
use App\Category;
use App\sale;
use App\User;
use App\Address;

class managementController extends Controller
{
    public function manage()
    {
        $res1= sale::orderBy('id', 'DESC')->get();
        if(!$res1)
        {
            return view('admin_panel.dashboard.orderManagement')->with('all',[])
            ->with('products',[])
            ->with('sale',[]);
        }

        $cart=[];
        $product=[];
        $users=[];
        foreach($res1 as $r )
        {
            // echo "select * from users inner join addresses on users.address_id = addresses.id where users.id = $r->user_id" .'<br>';
            $datas[] = DB::select( DB::raw("
            select
                users.id as id ,
                users.full_name as full_name ,
                addresses.area as area ,
                addresses.city as city ,
                addresses.zip as zip,
                users.created_at
            from users inner join addresses on users.address_id = addresses.id where users.id = $r->user_id" ) )[0];

            $totalCart = explode(',',$r->product_id);
            foreach($totalCart as $c)
            {
                $cart[]=array_prepend(explode(':',$c), $r->id);
                $a=explode(':',$c);
                $res = Product::find($a[0]);
                $product[]=$res;
            }
        }

        $output = array();
        foreach ($datas as $values) {
            $values = (array) $values;
            $key = $values['id'];
            $output[$key] = $values;
        }

        // Don't want the referenceUid in the keys? Reset them:
        $output = array_values($output);
        $users = $this->paginate($output);

        return view('admin_panel.orders.index')->with('all',$cart)
        ->with('products',$product)
        ->with('sale',$res1)
        ->with('users',$users->withPath('/admin_panel/management'))
        ->with('status',['ditempatkan','diproses','diterima','batal']);
    }

    public function paginate($items, $perPage = 3, $page = NULL, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function update(Request $r)
    {
        $n=sale::find($r->orderId);

        if($n)
        {
            $n->order_status=$r->stat;
            $n->save();
        }

        $res1= sale::all();
        if(!$res1)
        {
            return view('admin_panel.dashboard.orderManagement')->with('all',[])
            ->with('products',[])
            ->with('sale',[]);
        }

        $cart=[];
        $product=[];
        foreach($res1 as $r )
        {
            $totalCart = explode(',',$r->product_id);
            foreach($totalCart as $c)
            {
                $cart[]=array_prepend(explode(':',$c), $r->id);
                $a=explode(':',$c);
                $res = Product::find($a[0]);
                $product[]=$res;
            }
        }
        return redirect()->route('admin.orderManagement');

    }
}
