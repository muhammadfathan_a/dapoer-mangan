<?php

namespace App\Http\Livewire\AdminPanel;

use Livewire\Component;
use Livewire\WithPagination;
use App\Category;

class CategoriesComponent extends Component
{
    use WithPagination;
    public $search;

    public function render()
    {
        if ($this->search != NULL)
        {
            $this->resetPage();
            $result = Category::where('name', 'like', '%'. $this->search .'%')->orderBy('id', 'DESC')->paginate(5);
        }
        else
        {
            $result = Category::orderBy('id', 'DESC')->paginate(5);
        }

        return view('livewire.admin-panel.categories-component')
            ->with('catlist', $result);
    }
}
