<?php

namespace App\Http\Livewire\AdminPanel;

use Livewire\Component;
use Livewire\WithPagination;
use App\Product;

class ProductsComponent extends Component
{
    use WithPagination;
    public $search;

    public function render()
    {
        if ($this->search != NULL)
        {
            $this->resetPage();
            $result = Product::where('name', 'like', '%'. $this->search .'%')->orderBy('id', 'DESC')->paginate(5);
        }
        else
        {
            $result = Product::orderBy('id', 'DESC')->paginate(5);
        }

        return view('livewire.admin-panel.products-component')
            ->with('prdlist', $result);
    }
}
